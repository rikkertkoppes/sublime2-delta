#Delta Sublime text plugin

##About
Increment or decrement numbers in your source code with a simple keystroke. Based on the feature found in FireBug.

##Usage
There are four commands available:
- increment
- decrement
- increment_much
- decrement_much

Add to your own keybindings if you wish, like so:

    [
        { "keys": ["alt+up"], "command": "increment" },
        { "keys": ["alt+down"], "command": "decrement" },
        { "keys": ["alt+shift+up"], "command": "increment_much" },
        { "keys": ["alt+shift+down"], "command": "decrement_much" }
    ]

##Credit
Created by Rikkert Koppes, inspired by FireBug