import sublime, sublime_plugin

class DeltaMixin():
  def run(self,edit,delta):
    sels = self.view.sel()  
    for sel in sels:  
      region,number = DeltaMixin.getNumber(self,sel)
      self.view.replace(edit,region,'%(number)d' % {"number":number+delta})

  #return the found region and found text
  def getNumber(self,region):
    word = self.view.extract_scope(region.begin())
    num = self.view.find(r'-?\d+',word.begin())
    number = float(self.view.substr(num))
    return num,number

class DecrementCommand(sublime_plugin.TextCommand,DeltaMixin):
  def run(self, edit):
    DeltaMixin.run(self,edit,-1)

class IncrementCommand(sublime_plugin.TextCommand,DeltaMixin):
  def run(self, edit):
    DeltaMixin.run(self,edit,1)

class DecrementMuchCommand(sublime_plugin.TextCommand,DeltaMixin):
  def run(self, edit):
    DeltaMixin.run(self,edit,-10)

class IncrementMuchCommand(sublime_plugin.TextCommand,DeltaMixin):
  def run(self, edit):
    DeltaMixin.run(self,edit,10)